Contents of this File
—------------————————
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


Introduction
------------
The Greensock module includes the greensock animation (GSAP) javascript library.



Requirements
------------
* Use of this module requires Libraries
* Use of this module requires the latest version of GSAP JavaScript Library. In most cases, TweenMax.min.js is all that is needed, as it includes the rest of the libraries. 



Installation
------------
* Install module as usual (See: https://drupal.org/documentation/install/modules-themes/modules-7)



Configuration
-------------
Using Drush
* Using drush command line tool download the Libraries module:
```
drush en libraries -y
```
* Using drush command line tool to automatically download the library into the sites/all/libraries folder:
```
drush ldl ScrollMagic
```

Manually
* Download, extract and place the Libraries API module in sites/all/modules — download url https://www.drupal.org/project/libraries
* Download, extract and place the ScrollMagic JavaScript library in sites/all/libraries — download url  https://github.com/janpaepke/ScrollMagic/tarball/master



Maintainers
-----------
Current Maintainers:
* Albert Volkman (albert-volkman) - https://www.drupal.org/u/albert-volkman
* Chris McGrath (chris-mcgrath) - https://www.drupal.org/u/chris-mcgrath
